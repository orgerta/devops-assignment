#!/bin/bash

set -e
DOCKER_IMAGE="orgerta/react-todo"
#DOCKER_USERNAME="orgerta"
HELM_CHART="assignmentchart"
HELM_RELEASE="assignment-release"
NAMESPACE="assignment"
OLD_PATH="assignmentchart/values.yaml"
NEW_PATH="assignmentchart/override.yaml"
DOCKER_TAG=$(head -n1 $NEW_PATH | cut -c 12-18)

git checkout main
git fetch origin main
if [ $DOCKER_TAG != $(git rev-parse --short=7 origin/main) ]; then
  DOCKER_TAG=$(git rev-parse --short=7 origin/main)
  docker build -t $DOCKER_IMAGE:$DOCKER_TAG ./react-todo-app
  #read -s -p "Enter the password for your dockerhub account: " password
  #echo "$password" | docker login -u "$DOCKER_USERNAME" --password-stdin
  docker tag $DOCKER_IMAGE:$DOCKER_TAG $DOCKER_IMAGE:$DOCKER_TAG
  docker push $DOCKER_IMAGE:$DOCKER_TAG
  echo "image_tag: $DOCKER_TAG" > $NEW_PATH 
  helm upgrade -f $OLD_PATH -f $NEW_PATH $HELM_RELEASE $HELM_CHART/ -n $NAMESPACE
  echo "Deployed the new version of the helm chart with new tagged image"
else
  echo "No new commit on origin main"
  exit 0
fi

