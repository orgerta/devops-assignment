#!/bin/bash
set -e
desired_word="Merge branch"
git checkout main
git pull
released_number=$(sed -n "1p" "react-todo-app/Release.txt" | cut -c 15-19)
released_hash=$(sed -n "2p" "react-todo-app/Release.txt" | cut -c 16-22)
commit_hash=$(git log --grep="$desired_word" --pretty=format:'%h' -1 main)

if [ "$released_hash" != "$commit_hash" ]; then 
    incremented_released_number=$(echo "$released_number" | awk -F. '{$NF = $NF + 1;} 1' OFS=.)
    sed -i "1s/${released_number}/${incremented_released_number}/" "react-todo-app/Release.txt"
    sed -i "2s/${released_hash}/${commit_hash}/" "react-todo-app/Release.txt"
    git add react-todo-app/Release.txt
    git commit -m "Update app version and latest commit on Release.txt file"
    git push
    echo "The app version and latest commit updated!"
else
  echo "No new merge found on main"
  exit 0
fi


    